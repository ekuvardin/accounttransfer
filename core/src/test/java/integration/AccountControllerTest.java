package integration;

import org.acctransfer.account.AccountException;
import org.acctransfer.account.IAccountService;
import org.acctransfer.account.inmemory.AccountInMemory;
import org.acctransfer.account.inmemory.AccountServiceInMemory;
import org.acctransfer.controllers.AccountController;
import org.junit.*;
import org.junit.rules.ExpectedException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class AccountControllerTest {

    private IAccountService accountService;
    private AccountController accountController;
    private Validator validator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Before
    public void init() {
        accountService = new AccountServiceInMemory(new ConcurrentHashMap<>());
        accountController = new AccountController(accountService);
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.validator = vf.getValidator();
    }

    /*---------------------------------------------------------------*/
    // Create account
    /*---------------------------------------------------------------*/

    @Test
    public void createAccountShouldFailedWhenAccountIdNotInitialized() throws AccountException {
        Set<ConstraintViolation<AccountInMemory>> violations = this.validator.validate(createAccountInMemory(null, "zero",  BigDecimal.ZERO));
        Assert.assertEquals(1, violations.size());
        Assert.assertEquals("Account identifier may not be null", violations.iterator().next().getMessage());
    }

    @Test
    public void createAccountShouldFailedWhenAmountNotInitialized() throws AccountException {
        Set<ConstraintViolation<AccountInMemory>> violations = this.validator.validate( createAccountInMemory("1","zero", null));
        Assert.assertEquals(1, violations.size());
        Assert.assertEquals("Account amount may not be null", violations.iterator().next().getMessage());
    }

    @Test
    public void createAccountShouldFailedWhenOwnerNotInitialized() throws AccountException {
        Set<ConstraintViolation<AccountInMemory>> violations = this.validator.validate(createAccountInMemory("1", null, BigDecimal.ZERO));
        Assert.assertEquals(1, violations.size());
        Assert.assertEquals("Account owner may not be null", violations.iterator().next().getMessage());
    }

    @Test
    public void createAccountShouldSuccess() throws AccountException {
        Response actual = accountController.createAccount(createAccountInMemory("1", "owner", BigDecimal.ZERO));
        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());
    }

    @Test
    public void createAccountShouldFailedIfAccountAlreadyExists() throws AccountException {
        Response actual = accountController.createAccount(createAccountInMemory("1", "owner", BigDecimal.ZERO));
        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());

        exception.expect(AccountException.class);
        exception.expectMessage(String.format("Account with the same id:%s already exists", 1));

        accountController.createAccount(createAccountInMemory("1", "ownerz", BigDecimal.TEN));
    }

    /*---------------------------------------------------------------*/
    // Get account
    /*---------------------------------------------------------------*/

    @Test
    public void getAccountShouldFailed() throws AccountException {
        Response actual = accountController.getAccount("123");
        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), actual.getStatus());
    }

    @Test
    public void getAccountShouldSuccess() throws AccountException {
        AccountInMemory account = createAccountInMemory("1", "owner", BigDecimal.ZERO);
        Response actual = accountController.createAccount(account);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());

        actual = accountController.getAccount("1");

        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());
        Assert.assertTrue(account.equals(actual.getEntity()));
    }

    /*---------------------------------------------------------------*/
    // Transfer account
    /*---------------------------------------------------------------*/

    @Test
    public void transferMoneyShouldFailedWhenTransferZero() throws AccountException {
        createTwoAccounts();

        exception.expect(AccountException.class);
        exception.expectMessage("Money should be positive and not equal to Zero");

        accountController.transferMoney("2","1", BigDecimal.ZERO);
    }

    @Test
    public void transferMoneyShouldFailedWhenTransferNegative() throws AccountException {
        createTwoAccounts();

        exception.expect(AccountException.class);
        exception.expectMessage("Money should be positive and not equal to Zero");

        accountController.transferMoney("2","1", BigDecimal.TEN.negate());
    }

    @Test
    public void transferMoneyShouldFailedWhenFromNotFound() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Account with id:2 not found");

        accountController.transferMoney("2","1", BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldFailedWhenToNotFound() throws AccountException {
        createTwoAccounts();

        exception.expect(AccountException.class);
        exception.expectMessage("Account with id:A42 not found");

        accountController.transferMoney("2","A42", BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldFailedWhenAccountsAreEqual() throws AccountException {
        createTwoAccounts();

        exception.expect(AccountException.class);
        exception.expectMessage("Accounts are equal");

        accountController.transferMoney("2","2", BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldFailedWhenNotEnoughMoney() throws AccountException {
        createTwoAccounts();

        exception.expect(AccountException.class);
        exception.expectMessage("Not enough money to withdraw");

        accountController.transferMoney("2","1", BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldSuccess() throws AccountException {
        createTwoAccounts();

        Response actual = accountController.transferMoney("2","1", BigDecimal.ONE);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());

        Object entity = accountController.getAccount("2").getEntity();
        Assert.assertEquals(AccountInMemory.class, entity.getClass());
        Assert.assertEquals(BigDecimal.ZERO, ((AccountInMemory)entity).getAmount());

        entity = accountController.getAccount("1").getEntity();
        Assert.assertEquals(AccountInMemory.class, entity.getClass());
        Assert.assertEquals(BigDecimal.ONE, ((AccountInMemory)entity).getAmount());

    }

    private AccountInMemory createAccountInMemory(String accountId, String owner, BigDecimal amount) {
        AccountInMemory account = new AccountInMemory();
        account.setAccountId(accountId);
        account.setOwner(owner);
        account.setAmount(amount);
        return account;
    }

    private void createTwoAccounts() throws AccountException {
        Response actual = accountController.createAccount(createAccountInMemory("1", "owner", BigDecimal.ZERO));
        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());

        actual = accountController.createAccount(createAccountInMemory("2", "owner", BigDecimal.ONE));
        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());
    }

}
