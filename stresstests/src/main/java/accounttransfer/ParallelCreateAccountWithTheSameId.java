package accounttransfer;

import org.acctransfer.account.AccountException;
import org.acctransfer.account.inmemory.AccountInMemory;
import org.acctransfer.account.inmemory.AccountServiceInMemory;
import org.openjdk.jcstress.annotations.*;
import org.openjdk.jcstress.infra.results.II_Result;

import java.util.concurrent.ConcurrentHashMap;


/**
 * Test possibility to create several accounts concurrently.
 * Check if only one thread succeed.
 */
@JCStressTest
@Outcome(id = "0, 0", expect = Expect.FORBIDDEN, desc = "None of the accounts were created")
@Outcome(id = "1, 0", expect = Expect.ACCEPTABLE, desc = "First account was created")
@Outcome(id = "0, 1", expect = Expect.ACCEPTABLE, desc = "Second account was created")
@Outcome(id = "1, 1", expect = Expect.FORBIDDEN, desc = "Both account were created")
@State
public class ParallelCreateAccountWithTheSameId {

    AccountServiceInMemory accountServiceInMemory = new AccountServiceInMemory(new ConcurrentHashMap<>());

    AccountInMemory accountInMemory1 = new AccountInMemory();
    AccountInMemory accountInMemory2 = new AccountInMemory();

    @Actor
    public void actor1(II_Result r) {
        accountInMemory1.setAccountId("1");
        try {
            accountServiceInMemory.createAccount(accountInMemory1);
            r.r1 = 1;
        } catch (AccountException e) {
            r.r1 = 0;
        }
    }

    @Actor
    public void actor2(II_Result r) {
        accountInMemory2.setAccountId("1");
        try {
            accountServiceInMemory.createAccount(accountInMemory2);
            r.r2 = 1;
        } catch (AccountException e) {
            r.r2 = 0;
        }
    }

}