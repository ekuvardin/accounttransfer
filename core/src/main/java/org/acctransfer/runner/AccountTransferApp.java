package org.acctransfer.runner;

import org.acctransfer.controllers.exceptions.AccountExceptionMapper;
import org.acctransfer.controllers.AccountController;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Application resource configuration
 */
public class AccountTransferApp extends ResourceConfig {

	public AccountTransferApp() {
		packages("org.acctransfer");
		register(new AppBinder());
		register(AccountController.class);
		register(AccountExceptionMapper.class);
	}
}
