package junit.org.acctransfer.account.inmemory;

import org.acctransfer.account.inmemory.AccountInMemory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class AccountInMemoryTest {

    @Test
    public void compareToShouldCallCompareOnAccountId(){
        AccountInMemory accountInMemory1 = new AccountInMemory();
        accountInMemory1.setAccountId("AAA");
        AccountInMemory accountInMemory2 = Mockito.mock(AccountInMemory.class);
        Mockito.when(accountInMemory2.getAccountId()).thenReturn("AAA");

        Assert.assertEquals(0, accountInMemory1.compareTo(accountInMemory2));

        Mockito.verify(accountInMemory2).getAccountId();
    }
}
