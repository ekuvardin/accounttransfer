package org.acctransfer.account.inmemory;

import org.acctransfer.account.IAccount;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/**
 * Simple implementation accounts in memory
 */
public class AccountInMemory implements IAccount {

    @NotNull(message = "Account amount may not be null")
    //  Do not know about sign of amount
    private volatile BigDecimal amount;

    // Do not know about constraint to length or should be empty
    @NotNull(message = "Account identifier may not be null")
    private String accountId;

    // Do not know about constraint to length or should be empty
    @NotNull(message = "Account owner may not be null")
    private String owner;

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public void deposit(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        this.amount = this.amount.add(amount.negate());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountInMemory that = (AccountInMemory) o;

        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
        return owner != null ? owner.equals(that.owner) : that.owner == null;
    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(IAccount o) {
        return this.accountId.compareTo(o.getAccountId());
    }

}
