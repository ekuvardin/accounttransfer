package accounttransfer;

import org.acctransfer.account.AccountException;
import org.acctransfer.account.inmemory.AccountInMemory;
import org.acctransfer.account.inmemory.AccountServiceInMemory;
import org.openjdk.jcstress.annotations.*;
import org.openjdk.jcstress.infra.results.II_Result;
import org.openjdk.jcstress.infra.results.I_Result;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Test possibility to facing deadlock or lose money when two simultaneous
 * transaction try to exchange money using the same accounts
 *
 * No deadlock should be observed and money should be exchanged
 */
@JCStressTest
@Outcome(id = "19, 1", expect = Expect.ACCEPTABLE, desc = "All transation was successful")
@State
public class TransferMoneyWillNotCauseToDeadlockAndMoneyExchange {

    AccountServiceInMemory accountServiceInMemory;

    AccountInMemory accountInMemory1;
    AccountInMemory accountInMemory2;

    public TransferMoneyWillNotCauseToDeadlockAndMoneyExchange() {
        accountServiceInMemory = new AccountServiceInMemory(new ConcurrentHashMap<>());
        accountInMemory1 = new AccountInMemory();
        accountInMemory1.setAccountId("1");
        accountInMemory1.setAmount(BigDecimal.TEN);

        accountInMemory2 = new AccountInMemory();
        accountInMemory2.setAccountId("2");
        accountInMemory2.setAmount(BigDecimal.TEN);

        try {
            accountServiceInMemory.createAccount(accountInMemory1);
            accountServiceInMemory.createAccount(accountInMemory2);
        } catch (AccountException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Actor
    public void actor1(){
        try {
            accountServiceInMemory.transferMoney("1", "2", BigDecimal.ONE);
        } catch (AccountException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Actor
    public void actor2() {
        try {
            accountServiceInMemory.transferMoney("2", "1", BigDecimal.TEN);
        } catch (AccountException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Arbiter
    public void arbiter(II_Result r) {
        r.r1 = accountServiceInMemory.findAccount("1").getAmount().intValue();
        r.r2 = accountServiceInMemory.findAccount("2").getAmount().intValue();
    }

}