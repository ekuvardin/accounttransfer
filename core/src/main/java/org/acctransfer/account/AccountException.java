package org.acctransfer.account;

/**
 * Business logic exception.
 */
public class AccountException extends Exception {

    public AccountException(String shortMessage){
        super(shortMessage);
    }
}
