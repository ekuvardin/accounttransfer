package junit.org.acctransfer.account.inmemory;

import org.acctransfer.account.inmemory.AccountServiceInMemory;
import org.acctransfer.account.AccountException;
import org.acctransfer.account.IAccount;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceInMemoryTest {

    private AccountServiceInMemory accountServiceInMemory;

    @Mock
    private Map<String, IAccount> map;

    @Mock
    private IAccount account;

    @Mock
    private IAccount accountTransfer;

    private final String accountID = "AAA";

    private final String accountTransferId = "BBB";

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        accountServiceInMemory = new AccountServiceInMemory(map);
        Mockito.when(account.getAccountId()).thenReturn(accountID);
        Mockito.when(account.getAmount()).thenReturn(BigDecimal.ONE);

        Mockito.when(accountTransfer.getAmount()).thenReturn(BigDecimal.ZERO);
    }

    @Test
    public void createAccountShouldCallPutIfAbsent() throws AccountException {
        accountServiceInMemory.createAccount(account);
        Mockito.verify(map).putIfAbsent(accountID, account);
    }

    @Test
    public void createAccountShouldThrowExceptionWhenAccountExists() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Account with the same id:AAA already exists");

        Mockito.when(map.putIfAbsent(accountID, account)).thenReturn(account);

        accountServiceInMemory.createAccount(account);
    }

    @Test
    public void findAccountsShouldCallMapGet() throws AccountException {
        Assert.assertNull(accountServiceInMemory.findAccount(accountID));
        Mockito.verify(map).get(accountID);
    }

    @Test
    public void findAccountsShouldReturnInstance() throws AccountException {
        Mockito.when(map.get(accountID)).thenReturn(account);
        Assert.assertNotNull(accountServiceInMemory.findAccount(accountID));
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenFromNotFound() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Account with id:AAA not found");

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.ZERO);
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenToNotFound() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Account with id:BBB not found");

        Mockito.when(map.get(accountID)).thenReturn(accountTransfer);

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.ZERO);
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenZeroMoney() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Money should be positive and not equal to Zero");

        Mockito.when(map.get(accountID)).thenReturn(account);
        Mockito.when(map.get(accountTransferId)).thenReturn(account);

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.ZERO);
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenNegativeMoney() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Money should be positive and not equal to Zero");

        Mockito.when(map.get(accountID)).thenReturn(account);
        Mockito.when(map.get(accountTransferId)).thenReturn(accountTransfer);

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.TEN.negate());
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenAccountsAreEqual() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Accounts are equal");

        Mockito.when(map.get(accountID)).thenReturn(account);

        accountServiceInMemory.transferMoney(accountID, accountID, BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldThrowExceptionWhenNotEnoughMoneyToWithdraw() throws AccountException {
        exception.expect(AccountException.class);
        exception.expectMessage("Not enough money to withdraw");

        Mockito.when(map.get(accountID)).thenReturn(account);
        Mockito.when(map.get(accountTransferId)).thenReturn(accountTransfer);

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.TEN);
    }

    @Test
    public void transferMoneyShouldBeSuccesed() throws AccountException {

        Mockito.when(map.get(accountID)).thenReturn(account);
        Mockito.when(map.get(accountTransferId)).thenReturn(accountTransfer);

        accountServiceInMemory.transferMoney(accountID, accountTransferId, BigDecimal.ONE);

        Mockito.verify(account).withdraw(BigDecimal.ONE);
        Mockito.verify(accountTransfer).deposit(BigDecimal.ONE);
    }
}
