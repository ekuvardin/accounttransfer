package org.acctransfer.account.inmemory;

import org.acctransfer.account.AccountException;
import org.acctransfer.account.IAccount;
import org.acctransfer.account.IAccountService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Simple implementations acctions with accounts in memory
 */
public class AccountServiceInMemory implements IAccountService {

    private final Map<String, IAccount> accounts;

    @Inject
    public AccountServiceInMemory(Map<String, IAccount> accounts){
        this.accounts = accounts;
    }

    @Override
    public void createAccount(IAccount account) throws AccountException {
        if (accounts.putIfAbsent(account.getAccountId(), account) != null)
            throw new AccountException(String.format("Account with the same id:%s already exists", account.getAccountId()));
    }

    @Override
    public void transferMoney(String from, String to, BigDecimal money) throws AccountException {
        IAccount fromAccount = findAccount(from);
        IAccount toAccount = findAccount(to);

        if (fromAccount == null)
            throw new AccountException(String.format("Account with id:%s not found", from));

        if (toAccount == null)
            throw new AccountException(String.format("Account with id:%s not found", to));

        if (money.compareTo(BigDecimal.ZERO) <= 0)
            throw new AccountException("Money should be positive and not equal to Zero");

        if (from.equals(to))
            throw new AccountException("Accounts are equal");

        transferMoney(fromAccount, toAccount, money);
    }

    private void transferMoney(IAccount from, IAccount to, BigDecimal money) throws AccountException {
        IAccount start, end;

        if (from.compareTo(to) < 0) {
            start = from;
            end = to;
        } else {
            start = to;
            end = from;
        }

        synchronized (start) {
            synchronized (end) {
                if (from.getAmount().add(money.negate()).compareTo(BigDecimal.ZERO) < 0) {
                    throw new AccountException("Not enough money to withdraw");
                }
                from.withdraw(money);
                to.deposit(money);
            }
        }
    }

    @Override
    public IAccount findAccount(String accountId) {
        return accounts.get(accountId);
    }
}
