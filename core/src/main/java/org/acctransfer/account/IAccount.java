package org.acctransfer.account;

import java.math.BigDecimal;

/**
 * Account
 */
public interface IAccount extends Comparable<IAccount> {

    /**
     * Get current balance
     *
     * @return current balance
     */
    BigDecimal getAmount();

    /**
     * Get account identifier
     *
     * @return account identifier
     */
    String getAccountId();

    /**
     * Get account owner
     *
     * @return account owner
     */
    String getOwner();

    /**
     * Deposit account balance
     *
     * @param amount amount to deposit
     */
    void deposit(BigDecimal amount);

    /**
     * Withdraw account balance
     *
     * @param amount amount to withdraw
     */
    void withdraw(BigDecimal amount);
}
