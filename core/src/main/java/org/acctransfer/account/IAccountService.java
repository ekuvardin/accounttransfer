package org.acctransfer.account;

import java.math.BigDecimal;

/**
 * Actions with accounts
 */
public interface IAccountService {

    /**
     * Create account
     *
     * @param account created account
     * @throws AccountException throws when some business rules are volatile
     */
    void createAccount(IAccount account) throws AccountException;

    /**
     * Transfer money fron one account to another
     *
     * @param from  account to withdraw
     * @param to    account to deposit
     * @param money money to send
     * @throws AccountException throws when some business rules are volatile
     */
    void transferMoney(String from, String to, BigDecimal money) throws AccountException;

    /**
     * Find account by identifier
     *
     * @param accountId account identifier
     * @return finded account or null otherwise
     */
    IAccount findAccount(String accountId);
}
