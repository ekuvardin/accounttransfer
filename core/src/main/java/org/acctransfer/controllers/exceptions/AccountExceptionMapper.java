package org.acctransfer.controllers.exceptions;

import org.acctransfer.account.AccountException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Handler to process business logic exception
 */
public class AccountExceptionMapper implements ExceptionMapper<AccountException> {

    @Override
    public Response toResponse(AccountException ex) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(ex.getMessage())
                .type(MediaType.TEXT_PLAIN).
                        build();
    }

}