package org.acctransfer.controllers;


import org.acctransfer.account.AccountException;
import org.acctransfer.account.IAccount;
import org.acctransfer.account.IAccountService;
import org.acctransfer.account.inmemory.AccountInMemory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Actions which available through rest
 */
@Path("/accounts")
@Singleton
public class AccountController {

    private org.acctransfer.account.IAccountService IAccountService;

    @Inject
    public AccountController(IAccountService IAccountService) {
        this.IAccountService = IAccountService;
    }

    /**
     * Transfer money fron one account to another
     *
     * @param accountFrom account to withdraw
     * @param accountTo   account to deposit
     * @param money       money to send
     * @return succeed response or failed
     * @throws AccountException throws when some business rules are volatile
     */
    @POST
    @Path("transfer/{accountFrom}/{accountTo}/{money}")
    public Response transferMoney(@PathParam("accountFrom") String accountFrom, @PathParam("accountTo") String accountTo, @PathParam("money") BigDecimal money) throws AccountException {
        IAccountService.transferMoney(accountFrom, accountTo, money);
        return Response.ok().build();
    }

    /**
     * Create account
     *
     * @param account created account
     * @return succeed response or failed
     * @throws AccountException throws when some business rules are volatile
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(@Valid AccountInMemory account) throws AccountException {
        IAccountService.createAccount(account);
        return Response.ok().build();
    }

    /**
     * Find account by identifier
     *
     * @param accountId account identifier
     * @return response with account fields or not found response
     * @throws AccountException throws when some business rules are volatile
     */
    @GET
    @Path("/{accountId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("accountId") String accountId) throws AccountException {
        IAccount account = IAccountService.findAccount(accountId);
        return account == null ?
                Response.status(Response.Status.NOT_FOUND).build() :
                Response.ok(IAccountService.findAccount(accountId)).build();
    }
}
