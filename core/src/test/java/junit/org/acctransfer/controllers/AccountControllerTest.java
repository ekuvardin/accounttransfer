package junit.org.acctransfer.controllers;

import org.acctransfer.controllers.AccountController;
import org.acctransfer.account.AccountException;
import org.acctransfer.account.IAccountService;
import org.acctransfer.account.inmemory.AccountInMemory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {

    private AccountController accountController;

    @Mock
    private IAccountService accountService;

    @Mock
    private AccountInMemory accountInMemory;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        accountController = new AccountController(accountService);
    }

    @Test
    public void transferMoneyShouldCallAccountService() throws AccountException {
        accountController.transferMoney("AAA", "BBB", BigDecimal.ZERO);
        Mockito.verify(accountService).transferMoney("AAA", "BBB", BigDecimal.ZERO);
    }

    @Test
    public void createAccountShouldCallAccountService() throws AccountException {
        accountController.createAccount(accountInMemory);

        Mockito.verify(accountService).createAccount(accountInMemory);
    }

    @Test
    public void getAccountShouldReturnNotFoundResponse() throws AccountException {
        Response actual = accountController.getAccount("AAA");
        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), actual.getStatus());

        Mockito.verify(accountService).findAccount("AAA");
    }

    @Test
    public void getAccountShouldReturnOkResponse() throws AccountException {
        Mockito.when(accountService.findAccount("AAA")).thenReturn(accountInMemory);

        Response actual = accountController.getAccount("AAA");

        Assert.assertEquals(Response.Status.OK.getStatusCode(), actual.getStatus());
        Assert.assertTrue(accountInMemory.equals(actual.getEntity()));
    }
}
