package org.acctransfer.runner;

import org.acctransfer.account.IAccountService;
import org.acctransfer.account.inmemory.AccountServiceInMemory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Dependency injection binder
 */
public class AppBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(new AccountServiceInMemory(new ConcurrentHashMap<>())).to(IAccountService.class);
    }
}
