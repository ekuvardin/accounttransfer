Project contains minimum requirements to support account transfer for in memory accounts due to keep it simple
and extends it late.

## Dependency

To successfully build and run project you will need

1. Java 8+
2. Maven 3.0

---

## Build

Next step will help you to run project

1. After clone repo in commandline navigate to main folder
2. Run "mvn clean install"
3. To run standalone server you should type "cd core" next "mvn jetty:run"
4. Now server is ready to get rest requests
5. Server listening on http://localhost:8680. Port can be changed in accounttransfer\core\pom.xml

---
## Project structure
1. Module core - source code and tests.

   		1.1. src - source code
		1.2. test - tests divided by integration and junit tests
			
2. Module stresstests - test some cases when need to check parallel code using framework jcstress (instruction to run - http://openjdk.java.net/projects/code-tools/jcstress/)	

## Rest request

Due to simplicity provided three methods

1.Create account

	POST http://localhost:8680/rest/accounts/ HTTP/1.1
	Accept-Encoding: gzip,deflate
	Content-Type: application/json
	Accept: application/json
	Content-Length: 54
	Host: localhost:8680
	Connection: Keep-Alive
	User-Agent: Apache-HttpClient/4.1.1 (java 1.5)
	
	json
	{
	"accountId":"23",
	"owner": "zero",
	"amount":23
	}
	
	Should return when success
	HTTP/1.1 200 OK

	Should return when account exists
	HTTP/1.1 500 Internal Server Error
    Account with the same id:23 already exists

2.Find account

    GET http://localhost:8680/rest/accounts/3 HTTP/1.1
    Accept-Encoding: gzip,deflate
    Accept: application/json
    Host: localhost:8680
    Connection: Keep-Alive
    User-Agent: Apache-HttpClient/4.1.1 (java 1.5)

    Should return when success
    HTTP/1.1 200 OK
    {"amount":23,"accountId":"23","owner":"zero"}

    Should return when account not found
    HTTP/1.1 404 Not Found

3.Transfer money

    POST http://localhost:8680/rest/accounts/transfer/23/3/21 HTTP/1.1
    Accept-Encoding: gzip,deflate
    Content-Type: application/json
    Accept: application/json
    Content-Length: 0
    Host: localhost:8680
    Connection: Keep-Alive
    User-Agent: Apache-HttpClient/4.1.1 (java 1.5)

    Should return when success
    HTTP/1.1 200 OK

    Or smt like this when erro occured
    HTTP/1.1 500 Internal Server Error

    Account with id:4 not found
